var Views = {
	REGISTRATION: 'registration',
	INSTRUCTIONS: 'instructions',
	STORY: 'story',
	NEW_SELECTION: 'new_selection',
	STORY_RESPONSE: 'story_response',
	SUMMARY: 'summary',
	THANK_YOU: 'thank_you',
	FEEDBACK: 'feedback'
};

var Game2 = function(options) {

	this.options = {
		mainDiv: null, // The main div
		loaderDiv: null, // The loader div
		views: {}, // View htmls
		currentView: null, // The current view
		nextStory: null, // The next story
		counterBoy: null //The counter for selecting random number
	};
	
	this.arr = [];
	while(this.arr.length < 5){
	    var randomnumber = Math.ceil(Math.random()*20)
	    if(this.arr.indexOf(randomnumber) > -1) continue;
	    this.arr[this.arr.length] = randomnumber;
	}

	this._preInit = function() {
		this.options = $.extend({}, this.options, options);
	};

	this._preInit();

	this._showLoader = function() {
		$('#' + this.options.mainDiv).hide();
		$('#' + this.options.loaderDiv).show();
	};

	this._hideLoader = function() {
		$('#' + this.options.loaderDiv).hide();
		$('#' + this.options.mainDiv).show();
	};

	this.init = function(divId) {
		this.render();
	};

	this.render = function(data) {
		var _this = this;
		if (!this.options.currentView) {
			this._showLoader();
			this.loadViews(function() {
				_this.options.currentView = Views.REGISTRATION;
				_this.showRegistration();
				_this._hideLoader();
			});
		}
		else {
			if (this.options.currentView == Views.INSTRUCTIONS) {
				this.showInstructions(data);
			}
			else if (this.options.currentView == Views.STORY_RESPONSE) {
				this.showResponse(data);
			}
			else if (this.options.currentView == Views.SUMMARY) {
				this.showSummary(data);
			}
			else if (this.options.currentView == Views.FEEDBACK) {
				this.showFeedback(data);
			}
			else if (this.options.currentView == Views.THANK_YOU) {
				this.showThankYou(data);
			}
			else if (this.options.currentView == Views.NEW_SELECTION) {
				this.showNewSelection(data);
			}
			else {
				this.showStory(data);
			}
			_this._hideLoader();
		}
	};

	this.loadViews = function(callback) {
		var _this = this;
		var count = { pages: 8 };
		this._loadPage(Views.REGISTRATION, 'views/registration.html', count, callback);
		this._loadPage(Views.INSTRUCTIONS, 'views/instructions.html', count, callback);
		this._loadPage(Views.STORY, 'views/story.html', count, callback);
		this._loadPage(Views.NEW_SELECTION, 'views/newselection.html', count, callback);
		this._loadPage(Views.STORY_RESPONSE, 'views/response.html', count, callback);
		this._loadPage(Views.SUMMARY, 'views/summary.html', count, callback);
		this._loadPage(Views.FEEDBACK, 'views/feedback.html', count, callback);
		this._loadPage(Views.THANK_YOU, 'views/thankyou.html', count, callback);
	};

	this._loadPage = function(id, url, count, callback) {
		var _this = this;
		$.get(url, function(data) {
			_this.options.views[id] = data;
			count.pages = count.pages - 1;
			if (count.pages == 0) {
				callback();
			}
		});
	};

	this.showResponse = function(data) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.STORY_RESPONSE]);
		$('#' + this.options.mainDiv).find('#explanation').html(data.explanation);
		var content = 'Your previous selections were '+localStorage.getItem('node1Value')+' and '+localStorage.getItem('node2Value');
		$('#' + this.options.mainDiv).find('#displayNodes').html(content);
		$('#' + this.options.mainDiv).find('#network').attr('src', 'images/' + data.id + '.png');
		if (data.id != 'sample') {
			$('#' + this.options.mainDiv).find('#expl-' + data.explanationId).show();
		}
		$('#' + this.options.mainDiv).find('#next').click(function() {
			if (data.explanation == 'Your chosen solution is optimal. No need for an explanation.')
				_this.options.currentView = Views.STORY;
			else
				_this.options.currentView = Views.NEW_SELECTION;
			_this._showLoader();
			if (data.id == 'sample') {
				_this.render(data);
			}
			else {
	    		$.ajax('/game/rating', {
	    		    data: JSON.stringify({
	    		    	id: data.id,
	    		    	code: data.code,
	    		    	threshold: data.threshold,
	    		    	explanationId: data.explanationId,
	    		    	value: $('#scale').val()
	    		    }),
	    		    contentType: 'application/json',
	    		    type: 'POST',
	    		    success: function(data) {
	    		    	_this.render(data);
	    		    }
	    		});
			}
		});
	};

	this.showStory = function(data) {
		var _this = this;
		if (!this.options.nextStory) {
			this._renderStory('sample', data.code);
			this.options.nextStory = this.arr[0];
			this.options.counterBoy=0;
		}
		else {
			if (this.options.counterBoy>=5)
				this._renderStory((this.options.counterBoy-5)+90, data.code);
			else
			{
				this._renderStory(this.arr[this.options.counterBoy], data.code);
			}
				this.options.counterBoy++;
		}
	};

	this._renderStory = function(id, codeVal, callback) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.STORY]);
		$('#' + this.options.mainDiv).find('#network').attr('src', 'images/' + id + '.png');
		if (id == 'sample') {
			$('#' + this.options.mainDiv).find('#counterboy').html(' - Sample');
    	}
		else
			$('#' + this.options.mainDiv).find('#counterboy').html(this.options.counterBoy+1);
		$('#' + this.options.mainDiv).find('#node1')
			.html(this._generateNodes('b'))
			.on('change', function() {
				var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
				var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
				if (node1== '-') {
					return;
				}
				$('#' + _this.options.mainDiv).find('#node2').html(_this._generateNodes(node1));
				$('#' + _this.options.mainDiv).find('#node2').val(node2);
			});
		$('#' + this.options.mainDiv).find('#node2')
			.html(this._generateNodes('a'))
			.on('change', function() {
				var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
				var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
				if (node2== '-') {
					return;
				}
				$('#' + _this.options.mainDiv).find('#node1').html(_this._generateNodes(node2));
				$('#' + _this.options.mainDiv).find('#node1').val(node1);
			});
		$('#' + this.options.mainDiv).find('#next').click(function() {
			var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
			var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
			localStorage.setItem("node1Value", node1);
			localStorage.setItem("node2Value", node2);
			if (node1 == '-' || node2== '-') {
				alert("Please select a valid node");
				return;
			}
			var node1id = node1.charCodeAt(0) - 'a'.charCodeAt(0);
			var node2id = node2.charCodeAt(0) - 'a'.charCodeAt(0);
	    	_this.options.currentView = Views.STORY_RESPONSE;
	    	if (id == 'sample') {
	    		_this.render({ id: 'sample', code: codeVal, node1: node1id, node2: node2id, explanation: 'You will see an explanation here.' });
	    	}
	    	else {
	    		_this._showLoader();
	    		$.ajax('/game/selection', {
	    		    data: JSON.stringify({
	    		    	id: id,
	    		    	code: codeVal,
	    		    	node1: node1id,
	    		    	node2: node2id
	    		    }),
	    		    contentType: 'application/json',
	    		    type: 'POST',
	    		    success: function(data) {
	    		    	if (_this.options.counterBoy > 7) {
	    		    		_this.options.currentView = Views.FEEDBACK;
	    		    	}
	    		    	else if (_this.options.counterBoy > 5) {
	    		    		_this.options.currentView = Views.STORY;
	    		    	}
	    		    	_this.render(data);
	    		    }
	    		});
	    	}
		});
	};

	this.showNewSelection = function(data) {
		var _this = this;
		var node1val = String.fromCharCode('a'.charCodeAt(0) + data.node1);
		var node2val = String.fromCharCode('a'.charCodeAt(0) + data.node2);
		$('#' + this.options.mainDiv).html(this.options.views[Views.NEW_SELECTION]);
		if (data.id == 'sample') {
			$('#' + this.options.mainDiv).find('#next').attr('value', 'Start Real Game');
		}
		$('#' + this.options.mainDiv).find('#network').attr('src', 'images/' + data.id + '.png');
		$('#' + this.options.mainDiv).find('#pnode1').html(node1val);
		if (data.id == 'sample') {
			$('#' + this.options.mainDiv).find('#counterboy').html(' - Sample');
    	}
		else
			$('#' + this.options.mainDiv).find('#counterboy').html(this.options.counterBoy);
		$('#' + this.options.mainDiv).find('#node1')
			.html(this._generateNodes(node2val))
			//.val(node1val)
			.on('change', function() {
				var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
				var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
				if (node1== '-') {
					return;
				}
				$('#' + _this.options.mainDiv).find('#node2').html(_this._generateNodes(node1));
				$('#' + _this.options.mainDiv).find('#node2').val(node2);
			});
		$('#' + this.options.mainDiv).find('#pnode2').html(node2val);
		$('#' + this.options.mainDiv).find('#node2')
			.html(this._generateNodes(node1val))
			//.val(node2val)
			.on('change', function() {
				var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
				var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
				if (node2== '-') {
					return;
				}
				$('#' + _this.options.mainDiv).find('#node1').html(_this._generateNodes(node2));
				$('#' + _this.options.mainDiv).find('#node1').val(node1);
			});
		$('#' + this.options.mainDiv).find('#next').click(function() {
			var node1 = $('#' + _this.options.mainDiv).find('#node1').val();
			var node2 = $('#' + _this.options.mainDiv).find('#node2').val();
			if (node1 == '-' || node2== '-') {
				alert("Please select a valid node");
				return;
			}
	    	_this.options.currentView = Views.STORY;
	    	if (data.id == 'sample') {
	    		_this.render(data);
	    	}
	    	else {
	    		_this._showLoader();
	    		$.ajax('/game/new-selection', {
	    		    data: JSON.stringify({
	    		    	id: data.id,
	    		    	code: data.code,
	    		    	threshold: data.threshold,
	    		    	explanationId: data.explanationId,
	    		    	node1: node1.charCodeAt(0) - 'a'.charCodeAt(0),
	    		    	node2: node2.charCodeAt(0) - 'a'.charCodeAt(0)
	    		    }),
	    		    contentType: 'application/json',
	    		    type: 'POST',
	    		    success: function(data) {
	    		    	alert(data.improvement);
	    		    	_this.options.currentView = Views.STORY;
	    				if (_this.options.counterBoy == 5) {
	    					_this.options.currentView = Views.SUMMARY;
	    				}
	    		    	_this.render(data);
	    		    }
	    		});
	    	}
		});
	};

	this._generateNodes = function(exclude) {
		var html = '<option value="' + '-' + '"> Please select </option>';
		for (var i = 0; i < 20; i++) {
			if (exclude != String.fromCharCode('a'.charCodeAt(0) + i)) {
				var char = String.fromCharCode('a'.charCodeAt(0) + i);
				html += '<option value="' + char + '">' + char + '</option>';
			}
		}
		return html;
	};

	this.showThankYou = function(data) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.THANK_YOU]);
		$('#' + this.options.mainDiv).find('#code').html(data.code);
	};

	this.showSummary = function(data) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.SUMMARY]);
		$('#' + this.options.mainDiv).find('#next').click(function() {
			var summary = $('#summary').val().trim();
			if (summary.length == 0) {
				alert("Please summarize your observations.");
				return;
			}
			_this._showLoader();
			$.ajax('/game/summary', {
			    data: JSON.stringify({
			    	code: data.code,
			    	text: $('#summary').val()
			    }),
			    contentType: 'application/json',
			    type: 'POST',
			    success: function() {
			    	_this.options.currentView = Views.STORY;
			    	_this.render(data);
			    }
			});
		});
	};

	this.showFeedback = function(data) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.FEEDBACK]);
		$('#' + this.options.mainDiv).find('#next').click(function() {
			_this._showLoader();
			$.ajax('/game/feedback', {
			    data: JSON.stringify({
			    	code: data.code,
			    	comments: $('#comments').val()
			    }),
			    contentType: 'application/json',
			    type: 'POST',
			    success: function(data) {
			    	_this.options.currentView = Views.THANK_YOU;
			    	_this.render(data);
			    }
			});
		});
	};

	this.showInstructions = function(data) {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.INSTRUCTIONS]);
		$('#' + this.options.mainDiv).find('#submit').click(function() {
			if (!$('#' + _this.options.mainDiv).find('#agreement').is(':checked')) {
				alert('Please read through the instructions!');
				return;
			}
	    	_this.options.currentView = Views.STORY;
	    	_this.render(data);
		});
	};

	this.showRegistration = function() {
		var _this = this;
		$('#' + this.options.mainDiv).html(this.options.views[Views.REGISTRATION]);
		$('#' + this.options.mainDiv).find('#submit').click(function() {
			_this.validateAndSubmit();
		});
	};

	this.validateAndSubmit = function() {
		var _this = this;
		var emailId = $('#' + this.options.mainDiv).find('input[name=emailId]').val().trim();
		var genders = $('#' + this.options.mainDiv).find('input[name=gender]');
		var gender = '';
		if (genders.is(':checked')) {
			gender = $('#' + this.options.mainDiv).find('input[name=gender]:checked').val().trim();
			if (gender == 'Other') {
				var otherGender = $('#' + this.options.mainDiv).find('input[name=otherGender]').val().trim();
				gender = gender + ' - ' + otherGender;
			}
		}
		var age = $('#' + this.options.mainDiv).find('select[name=age]').val().trim();
		if (age == 'NA') {
			alert('Please select an age group!');
			return;
		}
		this._showLoader();
		$.ajax('/game/user', {
		    data: JSON.stringify({
		    	emailId: emailId,
		    	gender: gender,
		    	age: age
		    }),
		    contentType: 'application/json',
		    type: 'POST',
		    success: function(data) {
		    	_this.options.currentView = Views.INSTRUCTIONS;
		    	_this.render(data);
		    }
		});
	};

};

$(document).ready(function() {

	var game2 = new Game2({
		mainDiv: 'wrapper',
		loaderDiv: 'loader'
	});
	game2.init();

});
