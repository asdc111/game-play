package edu.usc.teamcore.game2;

public class User {

	private String emailId;
	private String gender;
	private String age;

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [emailId=" + emailId + ", gender=" + gender + ", age=" + age + "]";
	}

}
