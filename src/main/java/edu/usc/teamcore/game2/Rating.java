package edu.usc.teamcore.game2;

public class Rating {

	private Integer id;
	private Integer explanationId;
	private Integer value;
	private Double threshold;

	private String code;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getExplanationId() {
		return explanationId;
	}

	public void setExplanationId(Integer id) {
		this.explanationId = id;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Rating [id=" + id + ", value=" + value + ", explanationId=" + explanationId + ", threshold=" + threshold + ", code=" + code + "]";
	}

	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
