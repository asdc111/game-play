package edu.usc.teamcore.game2;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("game")
public class GameConfig {

	private String gamesOutput;
	private String finalGameOutput;
	private String summaryOutput;
	private String feedBackOutput;

	public String getGamesOutput() {
		return gamesOutput;
	}

	public void setGamesOutput(String gamesOutput) {
		this.gamesOutput = gamesOutput;
	}

	public String getFinalGameOutput() {
		return finalGameOutput;
	}

	public void setFinalGameOutput(String finalGameOutput) {
		this.finalGameOutput = finalGameOutput;
	}

	public String getSummaryOutput() {
		return summaryOutput;
	}

	public void setSummaryOutput(String summaryOutput) {
		this.summaryOutput = summaryOutput;
	}

	public String getFeedBackOutput() {
		return feedBackOutput;
	}

	public void setFeedBackOutput(String feedBackOutput) {
		this.feedBackOutput = feedBackOutput;
	}

}
