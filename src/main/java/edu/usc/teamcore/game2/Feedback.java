package edu.usc.teamcore.game2;

public class Feedback {

	private String comments;
	private String code;

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Feedback [comments=" + comments + ", code="+ code+"]";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
