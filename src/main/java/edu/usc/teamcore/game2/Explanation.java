package edu.usc.teamcore.game2;

public class Explanation {

	private Integer id;
	private Integer explanationId;
	private String explanation;
	private Double threshold;
	
	private String code;

	public Explanation() { }

	public Explanation(Integer id, Integer explanationId, String explanation, String code) {
		this.id = id;
		this.explanationId = explanationId;
		this.explanation = explanation;
		this.code = code;
	}
	
	public Explanation(Integer id, Integer explanationId, String explanation, double threshold, String code) {
		this.id = id;
		this.explanationId = explanationId;
		this.explanation = explanation;
		this.threshold = threshold;
		this.code = code;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double id) {
		this.threshold = id;
	}

	public Integer getExplanationId() {
		return explanationId;
	}

	public void setExplanationId(Integer explanationId) {
		this.explanationId = explanationId;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	@Override
	public String toString() {
		return "Explanation [id=" + id + ", explanationId=" + explanationId + ", explanation=" + explanation + ", threshold="+threshold+ ", code="+code+ "]";
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
