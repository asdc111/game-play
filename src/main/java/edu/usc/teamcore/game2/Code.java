package edu.usc.teamcore.game2;

public class Code {

	private String code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Code [code=" + code + "]";
	}

}
