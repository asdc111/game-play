package edu.usc.teamcore.game2;

public class Selection {

	private Integer id;
	private Integer node1;
	private Integer node2;
	private String explanation;
	private Integer rating;
	private Integer explanationId;
	private Double threshold;
	private String code;
	
	private String improvement;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getExplanationId() {
		return explanationId;
	}

	public void setExplanationId(Integer id) {
		this.explanationId = id;
	}

	public Integer getNode1() {
		return node1;
	}

	public void setNode1(Integer node1) {
		this.node1 = node1;
	}

	public Integer getNode2() {
		return node2;
	}

	public void setNode2(Integer node2) {
		this.node2 = node2;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@Override
	public String toString() {
		return "Selection [id=" + id + ", node1=" + node1 + ", node2=" + node2 + ", explanation=" + explanation
				+ ", rating=" + rating + ", explanationId=" + explanationId + ", threshold=" +threshold+ ", improvement=" + improvement + ", code="+ code+ "]";
	}

	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	public String getImprovement() {
		return improvement;
	}

	public void setImprovement(String improvement) {
		this.improvement = improvement;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
