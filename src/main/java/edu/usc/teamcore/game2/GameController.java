package edu.usc.teamcore.game2;

import static java.lang.String.valueOf;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.opencsv.CSVWriter;

import helpers.CreateSolutions;
import helpers.Explainer;
import helpers.ExplanationBO;
import helpers.ShowImprovement;

@RestController
@RequestMapping("/game")
public class GameController {

	private static final Logger log = LoggerFactory.getLogger(GameController.class);

	private static final int MAX_LENGTH = 10;

	@Autowired
	private GameConfig conf;

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/user"
	)
	public Code updateUserDetails(@RequestBody User user, HttpServletRequest request) {
		Code code = new Code();
		code.setCode(RandomStringUtils.randomAlphanumeric(MAX_LENGTH).toUpperCase());
		request.getSession().setAttribute("user", user);
		request.getSession().setAttribute("counter", 0);
		log.info("User: {}", user);
		return code;
		
	}

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/selection"
	)
	public Explanation postSelection(@RequestBody Selection selection, HttpServletRequest request) throws IOException {
		User user = (User) request.getSession().getAttribute("user");
		log.info("User: {}, Selection: {}", user, selection);
		int counter  = (int)request.getSession().getAttribute("counter");
		if (counter++ < 5) {
			request.getSession().setAttribute("counter", counter);
			request.getSession().setAttribute("game-" + selection.getId(), selection);
			CreateSolutions sols = new CreateSolutions(selection.getId(), selection.getNode1(), selection.getNode2());
			Explainer explain = new Explainer(sols.UserSol, sols.OptSol, true);
			ExplanationBO explBo = explain.provideFirstDivergenceExplanation();
			log.info("Explanation: {}", explBo);
			selection.setExplanation(explBo.getText());
			return new Explanation(selection.getId(), explBo.getId(), explBo.getText(), explBo.getThreshold(), selection.getCode());
		}
		else {
			request.getSession().setAttribute("counter", counter);
			write(finalGameLock, conf.getFinalGameOutput(),
				selection.getCode(), new Date().toString(), user.getEmailId(), user.getGender(), user.getAge(), valueOf(selection.getId()), valueOf(selection.getNode1()), valueOf(selection.getNode2()));
			return new Explanation(selection.getId(), null, null, selection.getCode());
		}
	}

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/rating"
	)
	public Selection postRating(@RequestBody Rating rating, HttpServletRequest request) throws IOException {
		User user = (User) request.getSession().getAttribute("user");
		Selection selection = (Selection) request.getSession().getAttribute("game-" + rating.getId());
		log.info("User: {}, Selection: {}, Rating: {}", user, selection, rating);
		selection.setRating(rating.getValue());
		selection.setExplanationId(rating.getExplanationId());
		selection.setThreshold(rating.getThreshold());
		selection.setCode(rating.getCode());
		return selection;
	}

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/new-selection"
	)
	public Selection postNewSelection(@RequestBody Selection newSelection, HttpServletRequest request) throws IOException {
		User user = (User) request.getSession().getAttribute("user");
		Selection selection = (Selection) request.getSession().getAttribute("game-" + newSelection.getId());
		log.info("User: {}, Selection: {}, NewSelection: {}", user, selection, newSelection);
		
		ShowImprovement improve = new ShowImprovement(selection.getId(), selection.getExplanationId(), newSelection.getNode1(), newSelection.getNode2(), selection.getThreshold());
		String improvement = improve.generateImprovementString();
		
		selection.setImprovement(improvement);
		selection.setCode(newSelection.getCode());
		
		write(gamesLock, conf.getGamesOutput(),
				newSelection.getCode(), new Date().toString(), user.getEmailId(), user.getGender(), user.getAge(), valueOf(newSelection.getId()),
				valueOf(selection.getNode1()), valueOf(selection.getNode2()),
				valueOf(newSelection.getNode1()), valueOf(newSelection.getNode2()),
				valueOf(selection.getRating()), selection.getExplanation());
		request.getSession().removeAttribute("game-" + newSelection.getId());
		
		return selection;
	}

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/summary"
	)
	public void postSummary(@RequestBody Summary summary, HttpServletRequest request) throws IOException {
		User user = (User) request.getSession().getAttribute("user");
		log.info("User: {}, Summary: {}", user, summary);
		write(summaryLock, conf.getSummaryOutput(),
			summary.getCode(), new Date().toString(), user.getEmailId(), user.getGender(), user.getAge(), summary.getText());
	}

	@RequestMapping(
		method = RequestMethod.POST,
		value = "/feedback"
	)
	public Code postFeedback(@RequestBody Feedback feedback, HttpServletRequest request) throws IOException {
		Code code = new Code();
		code.setCode(feedback.getCode());
		User user = (User) request.getSession().getAttribute("user");
		log.info("User: {}, Code: {}, Feedback: {}", user, code, feedback);
		write(feedbackLock, conf.getFeedBackOutput(),
			code.getCode(), new Date().toString(), user.getEmailId(), user.getGender(), user.getAge(), feedback.getComments());
		request.getSession().removeAttribute("user");
		return code;
	}

	private static ReentrantLock gamesLock = new ReentrantLock();
	private static ReentrantLock finalGameLock = new ReentrantLock();
	private static ReentrantLock feedbackLock = new ReentrantLock();
	private static ReentrantLock summaryLock = new ReentrantLock();

	private void write(ReentrantLock lock, String filePath, String... values) throws IOException {
		lock.lock();
		try {
			FileWriter fileWriter = new FileWriter(filePath, true);
			CSVWriter writer = new CSVWriter(fileWriter);
			writer.writeNext(values);
			writer.close();
		}
		finally {
			lock.unlock();
		}
	}

}
