package helpers;

import java.io.File;
import java.io.IOException;

public class ParetoFrontier {
	public DecisionTree list[];
	
	public ParetoFrontier() throws IOException
	{
		String basedir = new File("static/dtrees/").getAbsolutePath();
		///there are 38 trees in the pareto frontier
		list = new DecisionTree[38];
		for (int i=0;i<38;i++)
		{
			list[i] = new DecisionTree(basedir+"/dtree"+(i+1)+".csv");
		}
	}
}
