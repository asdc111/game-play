package helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.MatrixIO;
import org.ejml.simple.SimpleMatrix;

public class DecisionTree {
	
	DenseMatrix64F dataset;
	SimpleMatrix data, curr;

	HashMap<Integer, Node> nodeIdsSeen;
	
	public Node root;
	
	public double accuracy, interpretability;
	
	public DecisionTree(String filename)
	{
		try
		{
			nodeIdsSeen = new HashMap<Integer, Node>();
			dataset = MatrixIO.loadCSV(filename);
			data = SimpleMatrix.wrap(dataset);
			
			int index=1;
			
			///first line contains values of accuracy and interpretability
			curr = data.extractVector(true, 0);
			accuracy = curr.get(2);
			interpretability = curr.get(3);
			
			///for root
			curr = data.extractVector(true, 1);
			
			///assuming root is not empty
			root = new Node(index, (int)curr.get(0), curr.get(1), curr.get(3), false);
			nodeIdsSeen.put(index++, root);
			
			for (int i=2;i<data.numRows();i++)
			{
				curr = data.extractVector(true,i);
				
				int parent = (int) curr.get(4);
				Node parentNode = nodeIdsSeen.get(parent);
				
				parentNode.successors = new Node[2];
				
				///////////checking left node
				//is current node leaf
				int feature = (int)curr.get(0);
				if (feature<0)
				{
					parentNode.successors[0] = new Node(index, -1, -1, curr.get(3), true);
					nodeIdsSeen.put(index++, parentNode.successors[0]);
				}
				else
				{
					parentNode.successors[0] =new Node(index, (int)curr.get(0), curr.get(1), curr.get(3), false);
					nodeIdsSeen.put(index++, parentNode.successors[0]);
				}
				
				///check the next row
				i++;
				
				
				///checking right node
				curr = data.extractVector(true,i);
				
				int nextparent = (int) curr.get(4);
				if (nextparent!=parent)
					System.out.println("How is it possible?");
				
				
				//is current node leaf
				feature = (int)curr.get(0);
				if (feature<0)
				{
					parentNode.successors[1] = new Node(index, -1, -1, curr.get(3), true);
					nodeIdsSeen.put(index++, parentNode.successors[1]);
				}
				else
				{
					parentNode.successors[1] =new Node(index, (int)curr.get(0), curr.get(1), curr.get(3), false);
					nodeIdsSeen.put(index++, parentNode.successors[1]);
				}
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public void printTree(Node n)
	{
		/*Iterator it = nodeIdsSeen.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        
	        System.out.println("Node number: "+pair.getKey());*/
	        
	        System.out.println(n.ID+" "+n.isLeaf+" "+n.feature+" "+n.splitVal+" "+n.classProb);
	        if (!n.isLeaf)
	        {
	        	printTree(n.successors[0]);
	        	printTree(n.successors[1]);
	        }	
	        
	    //}
	}
	
}
