package helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ShowImprovement {
	String featureList[] = {
			"Average Closeness to other nodes",
			"Average Connectivity",
			"Average Influence",
			"Average Popularity",
			"Average Popularity Strength",
			"Mutual Connections",
			"Avg. Density",
			"Avg Risk Factor",
			"Maximum Popularity",
			"Minimum Popularity",
			"Variance in Popularity",
			"Maximum Density",
			"Minimum Density",
			"Maximum Risk Factor",
			"Minimum Risk Factor",
			"Variance in Risk Factor",
			"Maximum Popularity Strength",
			"Minimum Popularity Strength",
			"Variance in Popularity Strength",
			"Cluster value"};
	
	
	int netNumber, explanationId;
	double OldSolution;
	public double UserSol[];
	
	public ShowImprovement(int netNumber, int explanationId, int A, int B, double OldSolution) throws IOException
	{
		this.netNumber = netNumber;
		this.explanationId = explanationId;
		this.OldSolution = OldSolution;
		
		String solutiondir = new File("static/solutions/").getAbsolutePath();
		try 
		{
			int index=0;
			/*Scanner sc1 = new Scanner(new File(solutiondir+"/POMDPSols/"+netNumber+".txt"));
			int pomdp1=sc1.nextInt();
			int pomdp2=sc1.nextInt();*/
			
			System.out.println("User Solution: "+A+" "+B);
			//System.out.println("POMDP Solution: "+pomdp1+" "+pomdp2);
		
			
			boolean usersol_set = false;
			
			Scanner sc = new Scanner(new File(solutiondir+"/UserSols/"+netNumber+".txt"));
			while(sc.hasNextLine())
			{
				String solutionNodes[] = sc.nextLine().split("\\s+");
				int first = Integer.parseInt(solutionNodes[0]);
				int second = Integer.parseInt(solutionNodes[1]);
				if (!usersol_set && ((first==A && second==B) || (first==B && second==A)))
				{
					usersol_set=true;
					UserSol = new double[solutionNodes.length-2];
					for (int i=2;i<solutionNodes.length;i++)
						UserSol[i-2]= Double.parseDouble(solutionNodes[i]);
				}
				
				/*if (!optsol_set && ((first==pomdp1 && second==pomdp2) || (first==pomdp2 && second==pomdp1)))
				{
					optsol_set=true;
					OptSol = new double[solutionNodes.length-2];
					for (int i=2;i<solutionNodes.length;i++)
						OptSol[i-2]= Double.parseDouble(solutionNodes[i]);
				}*/
				
				if (usersol_set)
					break;
			}
			sc.close();
			
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
	
	public String generateImprovementString()
	{
		String result="In your earlier solution, the value of "+featureList[explanationId-1]+" is "+OldSolution+"\n";
		result+="In your new solution, the value of "+featureList[explanationId-1]+" is "+UserSol[explanationId-1];
		
		return result;
		
	}

}
