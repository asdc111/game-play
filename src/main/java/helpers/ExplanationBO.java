package helpers;

public class ExplanationBO {

	private Integer id;
	private String text;
	private Double threshold;

	public ExplanationBO(Integer id, String text, Double thres) {
		this.id = id;
		this.text = text;
		this.threshold = thres;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double thres) {
		this.threshold = thres;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}


	@Override
	public String toString() {
		return "ExplanationBO [id=" + id + ", text=" + text + ", threshold=" + threshold + "]";
	}

}
