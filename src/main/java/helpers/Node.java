package helpers;

public class Node {
	public int ID;
	
	public int feature;
	public double splitVal;	
	public boolean isLeaf;
	public double classProb;
	public Node successors[];
	

	
	public Node(int id, int feature, double splitVal, double classProb, boolean isleaf)
	{
		this.ID = id;
		this.classProb = classProb;

		this.isLeaf = isleaf;
		this.feature = feature;
		this.splitVal = splitVal;
	}
}
