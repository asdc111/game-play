package helpers;

import java.io.IOException;
import java.util.ArrayList;

public class Explainer {
	String featureList[] = {
	"Average Closeness to other nodes",
	"Average Connectivity",
	"Average Influence",
	"Average Popularity",
	"Average Popularity Strength",
	"Mutual Connections",
	"Avg. Density",
	"Avg Risk Factor",
	"Maximum Popularity",
	"Minimum Popularity",
	"Variance in Popularity",
	"Maximum Density",
	"Minimum Density",
	"Maximum Risk Factor",
	"Minimum Risk Factor",
	"Variance in Risk Factor",
	"Maximum Popularity Strength",
	"Minimum Popularity Strength",
	"Variance in Popularity Strength",
	"Cluster value"};
	
	double interpretWeights[] = {0.7122, 0, 0.2878,1.0000, 1.0000, 1.0000, 0.2574, 0.8637, 1.0000, 1.0000, 1.0000, 0.2574, 0.2574, 0.8637, 0.8637, 0.8637, 1.0000, 1.0000, 1.0000, 1.0000};
	
	ParetoFrontier pareto;
	double UserSol[], OptSol[];
	DecisionTree bestDtree;
	DecisionTree SecondApproach_BestDtree;
	boolean bestDtreeFound;
	
	ArrayList<DecisionTree> feasibleTrees;
	
	
	
	DecisionTree getMostInterpretableTree()
	{
		DecisionTree result=null;
		double maxInterpret = -1;
		int maxIndex = -1;
		for (int i=0;i<feasibleTrees.size();i++)
		{
			if (feasibleTrees.get(i).interpretability>maxInterpret)
			{
				maxInterpret = feasibleTrees.get(i).interpretability;
				maxIndex = i;
			}
		}
		if (maxIndex!=-1)
		{	
			result = feasibleTrees.get(maxIndex);
			bestDtreeFound=true;
		}
		
		return result;
	}
	
	/////////THESE TWO FUNCTIONS ARE FOR SECOND VARIANT OF GAME
	DecisionTree getMostInterpretableTree_SingleFeature()
	{
		DecisionTree result=null;
		double maxInterpret = -1;
		int maxIndex = -1;
		for (int i=0;i<feasibleTrees.size();i++)
		{
			int divergentFeature = findDivergentFeature(feasibleTrees.get(i));
			if (interpretWeights[divergentFeature-1]>maxInterpret)
			{
				maxInterpret = interpretWeights[divergentFeature-1];
				maxIndex = i;
			}
		}
		if (maxIndex!=-1)
		{	
			result = feasibleTrees.get(maxIndex);
			bestDtreeFound=true;
		}
		
		return result;
	}
	
	public Integer findDivergentFeature(DecisionTree Original)
	{	
		Node tree = Original.root;
		while(tree.isLeaf==false)
		{
			if (UserSol[tree.feature-1]<tree.splitVal && OptSol[tree.feature-1]<tree.splitVal)
				tree = tree.successors[0];
			else if (UserSol[tree.feature-1]>=tree.splitVal && OptSol[tree.feature-1]>=tree.splitVal)
				tree = tree.successors[1];
			else//we have a divergence
			{
				return tree.feature;
			}
		}
		System.out.println("Something really wrong is going on.");
		return -1;
	}
	///////////////////////////////THESE TWO FUNCTIONS END
	
	public Explainer(double UserSol[], double OptSol[], boolean isProbabilityMethod) throws IOException
	{
		bestDtreeFound = false;
		pareto = new ParetoFrontier();
		this.UserSol = UserSol;
		this.OptSol = OptSol;
		
		feasibleTrees = new ArrayList<DecisionTree>();
		for (int i=0;i<pareto.list.length;i++)
		{
			DecisionTree curr = pareto.list[i];
			int predictionOnUserSol=-1, predictionOnOptSol=-1;
			if (isProbabilityMethod)
			{
				double pred1 = predictDouble(UserSol, curr);
				double pred2 = predictDouble(OptSol, curr);
				
				if (pred2>pred1)
				{
					predictionOnOptSol=1;
					predictionOnUserSol=0;
				}
				else
				{
					predictionOnOptSol=0;
					predictionOnUserSol=1;
				}
			}
			else
			{
				predictionOnUserSol = predict(UserSol, curr);
				predictionOnOptSol = predict(OptSol, curr);
			}
			
			
			if (predictionOnUserSol==0 && predictionOnOptSol==1)
				feasibleTrees.add(curr);
		}
		
		if (feasibleTrees.size()==0)
			System.out.println("No feasible trees found");
		
		
		SecondApproach_BestDtree = getMostInterpretableTree_SingleFeature();
		bestDtree = getMostInterpretableTree();

	}
	
	int predict(double featureVec[], DecisionTree tree)
	{
		Node curr = tree.root;
		while (curr.isLeaf==false)
		{
			if (featureVec[curr.feature-1]<curr.splitVal)
				curr = curr.successors[0];
			else
				curr = curr.successors[1];
		}
		
		if (curr.classProb>0.5)
			return 1;
		else
			return 0;
	}
	
	double predictDouble(double featureVec[], DecisionTree tree)
	{
		Node curr = tree.root;
		while (curr.isLeaf==false)
		{
			if (featureVec[curr.feature-1]<curr.splitVal)
				curr = curr.successors[0];
			else
				curr = curr.successors[1];
		}
		
		return curr.classProb;
	}
	
	public ExplanationBO provideFirstDivergenceExplanation()
	{
		String result="";
		int resultInt=0;
		double resultDouble = 0.0;
		if (bestDtreeFound)
		{
			double pred1 = predictDouble(UserSol, bestDtree);
			double pred2 = predictDouble(OptSol, bestDtree);
			
			String userExplanation="";
			Node tree = bestDtree.root;
			while(tree.isLeaf==false)
			{
				if (UserSol[tree.feature-1]<tree.splitVal && OptSol[tree.feature-1]<tree.splitVal)
					tree = tree.successors[0];
				else if (UserSol[tree.feature-1]>=tree.splitVal && OptSol[tree.feature-1]>=tree.splitVal)
					tree = tree.successors[1];
				else//we have a divergence
				{
					/////comment out for 8080 game
					/*resultDouble  = UserSol[tree.feature-1];
					resultInt = tree.feature;
					while(tree.isLeaf==false)
					{
						if (UserSol[tree.feature-1]<tree.splitVal)
						{
							userExplanation += "If for a choice of 2 nodes, " +featureList[tree.feature-1] + " is strictly less than "+tree.splitVal+ "(for your chosen solution, the value was " + UserSol[tree.feature-1]".";
						}
					}*/
					
					resultDouble  = UserSol[tree.feature-1];
					resultInt = tree.feature;
					result +="Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]+". ";
					System.out.println("Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]);
					result += "On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]+". ";
					System.out.println("On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]);
					if (UserSol[tree.feature-1]<tree.splitVal)
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal);
					}
					else
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal);
					}
					break;
				}
			}
			
		}
		else
		{
			System.out.println("No decision tree found which separates optimal from user solution.");
		}
		if (result.compareTo("")==0)
			result+="Your chosen solution is optimal. No need for an explanation.";
		
		return new ExplanationBO(resultInt, result, resultDouble);
	}
	
	public ExplanationBO provideFirstDivergenceExplanation_SingleFeature()
	{
		String result="";
		int resultInt=0;
		double resultDouble = 0.0;
		if (bestDtreeFound)
		{
			double pred1 = predictDouble(UserSol, SecondApproach_BestDtree);
			double pred2 = predictDouble(OptSol, SecondApproach_BestDtree);
			
			String userExplanation="";
			Node tree = SecondApproach_BestDtree.root;
			while(tree.isLeaf==false)
			{
				if (UserSol[tree.feature-1]<tree.splitVal && OptSol[tree.feature-1]<tree.splitVal)
					tree = tree.successors[0];
				else if (UserSol[tree.feature-1]>=tree.splitVal && OptSol[tree.feature-1]>=tree.splitVal)
					tree = tree.successors[1];
				else//we have a divergence
				{
					/////comment out for 8080 game
					/*resultDouble  = UserSol[tree.feature-1];
					resultInt = tree.feature;
					while(tree.isLeaf==false)
					{
						if (UserSol[tree.feature-1]<tree.splitVal)
						{
							userExplanation += "If for a choice of 2 nodes, " +featureList[tree.feature-1] + " is strictly less than "+tree.splitVal+ "(for your chosen solution, the value was " + UserSol[tree.feature-1]".";
						}
					}*/
					
					resultDouble  = UserSol[tree.feature-1];
					resultInt = tree.feature;
					result +="Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]+". ";
					System.out.println("Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]);
					result += "On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]+". ";
					System.out.println("On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]);
					if (UserSol[tree.feature-1]<tree.splitVal)
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal);
					}
					else
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal);
					}
					break;
				}
			}
			
		}
		else
		{
			System.out.println("No decision tree found which separates optimal from user solution.");
		}
		if (result.compareTo("")==0)
			result+="Your chosen solution is optimal. No need for an explanation.";
		
		return new ExplanationBO(resultInt, result, resultDouble);
	}
	
	
	public ExplanationBO provideFullExplanation()
	{
		String result="";
		int resultInt=0;
		double resultDouble = 0.0;
		if (bestDtreeFound)
		{
			double pred1 = predictDouble(UserSol, bestDtree);
			double pred2 = predictDouble(OptSol, bestDtree);
		
			
			Node tree = bestDtree.root;
			while(tree.isLeaf==false)
			{
				if (UserSol[tree.feature-1]<tree.splitVal && OptSol[tree.feature-1]<tree.splitVal)
					tree = tree.successors[0];
				else if (UserSol[tree.feature-1]>=tree.splitVal && OptSol[tree.feature-1]>=tree.splitVal)
					tree = tree.successors[1];
				else//we have a divergence
				{
					///just return stuff for first divergence point
					resultDouble  = UserSol[tree.feature-1];
					resultInt = tree.feature;
					
					///////////////////GENERATE STRING FOR WHY USER SOLUTION IS SUBOPTIMAL
					String userOptimalExplanation="If for a choice of 2 nodes, ";
					String userSelfExplanation="Therefore, your solution is suboptimal because the ";
					Node userSolNode=null;
					if (UserSol[tree.feature-1]>=tree.splitVal)
					{
						userSolNode = tree.successors[1];
						userOptimalExplanation += featureList[tree.feature-1] + " is greater than or equal to "+tree.splitVal;
						userSelfExplanation += featureList[tree.feature-1]+" has a value of "+UserSol[tree.feature-1];
					}
					else
					{
						userSolNode = tree.successors[0];
						userOptimalExplanation += featureList[tree.feature-1] + " is strictly less than "+tree.splitVal;
						userSelfExplanation += featureList[tree.feature-1]+" has a value of "+UserSol[tree.feature-1];
					}
					while(userSolNode.isLeaf==false)
					{
						if (UserSol[userSolNode.feature-1]<userSolNode.splitVal)
						{
							userOptimalExplanation += ", AND " +featureList[userSolNode.feature-1] + " is strictly less than "+ userSolNode.splitVal;
							userSelfExplanation += ", AND " + featureList[userSolNode.feature-1]+" has a value of "+UserSol[userSolNode.feature-1];
							userSolNode = userSolNode.successors[0];
						}
						else
						{
							userOptimalExplanation += ", AND " +featureList[userSolNode.feature-1] + " is greater than or equal to "+userSolNode.splitVal;
							userSelfExplanation += ", AND " + featureList[userSolNode.feature-1]+" has a value of "+UserSol[userSolNode.feature-1];
							userSolNode = userSolNode.successors[1];
						}
					}
					userOptimalExplanation += ", then that choice of 2 nodes is suboptimal. ";
					
					String userExplain  = userOptimalExplanation + userSelfExplanation;
					
					
					
					///////////////////GENERATE STRING FOR WHY POMDP SOLUTION IS OPTIMAL
					String POMDPOptimalExplanation="On the other hand, for a solution to be optimal, ";
					Node OptSolNode=null;
					if (OptSol[tree.feature-1]>=tree.splitVal)
					{
						OptSolNode = tree.successors[1];
						POMDPOptimalExplanation += featureList[tree.feature-1] + " is greater than or equal to "+tree.splitVal;
					}
					else
					{
						OptSolNode = tree.successors[0];
						POMDPOptimalExplanation += featureList[tree.feature-1] + " is strictly less than "+tree.splitVal;
						//userSelfExplanation += featureList[tree.feature-1]+" has a value of "+UserSol[tree.feature-1];
					}
					while(OptSolNode.isLeaf==false)
					{
						if (OptSol[OptSolNode.feature-1]<OptSolNode.splitVal)
						{
							POMDPOptimalExplanation += ", AND " +featureList[OptSolNode.feature-1] + " is strictly less than "+ OptSolNode.splitVal;
							//userSelfExplanation += ", AND " + featureList[OptSolNode.feature-1]+" has a value of "+OptSol[OptSolNode.feature-1];
							OptSolNode = OptSolNode.successors[0];
						}
						else
						{
							POMDPOptimalExplanation += ", AND " +featureList[OptSolNode.feature-1] + " is greater than or equal to "+OptSolNode.splitVal;
							//userSelfExplanation += ", AND " + featureList[userSolNode.feature-1]+" has a value of "+UserSol[userSolNode.feature-1];
							userSolNode = userSolNode.successors[1];
						}
					}
					POMDPOptimalExplanation += ". ";
					
					userExplain  += POMDPOptimalExplanation;
					
					break;
					//System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal);
				}
				
			}
		}
		else
		{
			System.out.println("No decision tree found which separates optimal from user solution.");
		}
		if (result.compareTo("")==0)
			result+="Your chosen solution is optimal. No need for an explanation.";
		
		return new ExplanationBO(resultInt, result, resultDouble);
	}
	
}
